<?php


namespace App\Tests;

use PHPUnit\Framework\TestCase;

class OfferListTest  extends TestCase
{

   
    
    public function testGetAllOfferByCode($code = 'EKWA_WELCOME')
    {

        $url = 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList';

    	$offerList = json_decode(file_get_contents($url), true);


    	foreach ($offerList as $values) {

            $this->assertContains($code, $values['validPromoCodeList']);
            
        }

    }

}