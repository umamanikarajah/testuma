<?php

namespace App\Service;

class PromoCodeList
{

    /**
     * @var string
     */
    protected $url;


    /**
     * PromoCodeList constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Function to check a code in the json file
     * @param $code
     * @return false|array
     */
    public function check($code)
    {
        $codeList = json_decode(file_get_contents($this->url), true);

        foreach ($codeList as $values) {

            if (in_array(trim($code), $values)) {

                if ($this->validDate($values['endDate']) == true) {
                    return $values;
                }
            }
        }

        return false;
    }


    /**
     * check if the date is valid
     * @param $date
     * @return bool
     */
    public function validDate($date)
    {
        $today = strtotime(date('Y-m-d'));
        $check = strtotime($date);
        if ($check >= $today) {
            return true;
        }
        return false;
    }
}