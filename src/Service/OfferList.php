<?php


namespace App\Service;


class OfferList
{

    /**
     * @var string
     */
	protected $url;

    /**
     * OfferList constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Function to check a offer in the json file
     * @param $code
     * @return array
     */
    public function getAllOfferByCode($code)
    {
    	$offerList = json_decode(file_get_contents($this->url), true);

    	$tab = array();

    	foreach ($offerList as $values) {

            if(in_array(trim($code), $values['validPromoCodeList'])) {

                 $tab[] = array('name' => $values['offerName'], 'type' => $values['offerType']);
            }
        }

    	return $tab;
    }

}