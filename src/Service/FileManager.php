<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FileManager
{

	/**
     * @var array
     */
	protected $data;

	/**
     * @var ParameterBagInterface
     */
	protected $params;


    /**
     * FileManager constructor.
     * @param array $data
     * @param ParameterBagInterface $params
     */
    public function __construct(array $data, ParameterBagInterface $params)
    {
        $this->data 	= $data;
        $this->params 	= $params;
    }


    /**
     * Function to create a json file
     * @return false|int
     */
    public function excecute()
    {
    	$path 		= $this->params->get('fileDirectory');
        
        $path 		= str_replace('\\', '/', $path);

        $fileName 	= $path.'code_promo_'.time().'.json';

        $data 		= json_encode($this->data);

        return file_put_contents($fileName, $data);
    }

}


