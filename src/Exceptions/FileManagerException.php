<?php

namespace App\Exceptions;

use Exception;


class FileManagerException extends Exception
{

	/**
     * @var string
     */
    protected $message = '** Erreur : Could not create the Json File. **';


}