<?php


namespace App\Command;


use App\Service\PromoCodeList;
use App\Service\OfferList;
use App\Service\FileManager;
use App\Exceptions\FileManagerException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PromoCodeCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'promo-code:validate';

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * PromoCodeCommand constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
        parent::__construct();
    }


    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('code', InputArgument::REQUIRED, 'Which promo code do you want to test?')
        ;
    }



    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $json               = array();

        $code               = $input->getArgument('code');
        
        $promoCodeListUrl   = $this->params->get('promoCodeListUrl');
        
        $promoCodeList      = new PromoCodeList($promoCodeListUrl);
        
        $promoCodeArray     = $promoCodeList->check($code);

        if (is_array($promoCodeArray)) {

            $offerListUrl           = $this->params->get('offerListUrl');

            $OfferList              = new OfferList($offerListUrl);

            $fileList               = $OfferList->getAllOfferByCode($code);

            $json['promoCode']      = $promoCodeArray['code'];
            
            $json['endDate']        = $promoCodeArray['endDate'];
            
            $json['discountValue']  = $promoCodeArray['discountValue'];

            foreach ($fileList as $value) { 
                
                $json['compatibleOfferList'][] = array('name' => $value['name'], 'type' => $value['type']);
            }

            $fileManager    = new FileManager($json, $this->params);

            if ($fileManager->excecute() !== false)
                $output->writeln('** File create with succes ** ');
            else
                throw new FileManagerException;
        } else {
            
            $output->writeln('** Code promo not found ** ');
        }

        return Command::SUCCESS;
    }
}